/* =========================================================
 * Main loops
 * =========================================================
 */

import grafica.*;
import g4p_controls.*;
import processing.serial.*;
Serial myPort; // Creates the software serial port for Arduino comms
Table cycleTable; // Table where we will read in and store values for data
Table fullTable; // Table for save to csv
long readingCounter = 0; // Counts each reading to compare to numReadings. 
long seconds = 0;
int cycles = 0;
double J = 0; // Joules
double C = 0; // Coulombs
float mA; // milliamps
float Volts; // Volts
String timestamp = str(year()) +"_"+ str(month()) +"_"+ str(day()) +"_"+ str(hour()) +"_"+ str(minute());
String fileName = timestamp;
String ArduinoStr;
float refreshRate = 10;
float mAtoAmps = 1000;
boolean justSwitched = true;
long lastSwitched = 0;

double Cpre = 0;
double deltaC = 600;
double deltaU = 600;
int plotPeriod = 100;
int savePeriod = 300;
int DISPLAYED_CYCLES = 20;

String doState = "off";
int relay1 = 2;
int relay2 = 4;
GPlot Cplot;
GPlot Vplot;
GPlot Aplot;
GPointsArray Cpts;
GPointsArray Vpts;
GPointsArray Apts;

void setup() {
  size(1300, 700);
  frameRate(10);
  
  Cpts= new GPointsArray();
  Cplot = new GPlot(this);
  Cplot.setOuterDim(600,700);
  Cplot.setPos(0,0);
  Cplot.setTitleText("DisCharge");
  Cplot.getXAxis().setAxisLabelText("s");
  Cplot.getYAxis().setAxisLabelText("C");
  Cplot.setVerticalAxesNTicks(16);
  Cplot.setHorizontalAxesNTicks(12);
  
  Vpts = new GPointsArray();
  Vplot = new GPlot(this);
  Vplot.setOuterDim(600,700);
  Vplot.setPos(600,0);
  Vplot.setTitleText("Voltage (blue); Current (red)");
  Vplot.getXAxis().setAxisLabelText("s");
  Vplot.getYAxis().setAxisLabelText("V");
  Vplot.setVerticalAxesNTicks(12);
  Vplot.setHorizontalAxesNTicks(12);
  
  Apts = new GPointsArray();
  Aplot = new GPlot(this);
  Aplot.setOuterDim(600,700);
  Aplot.setPos(600,0);
  Aplot.getRightAxis().setAxisLabelText("mA");
  Aplot.getRightAxis().setDrawTickLabels(true);
  Aplot.setVerticalAxesNTicks(12);
  Aplot.setHorizontalAxesNTicks(12);
  
  createGUI();
  deltaC_text.setText( str((float) deltaC) );
  deltaU_text.setText( str((float) deltaU) );
  plotPeriod_text.setText( str(plotPeriod/10) );
  savePeriod_text.setText( str(savePeriod/10) );
  
  myPort = new Serial(this, "COM3", 57600); // Set up the port to listen to the serial port
  
  cycleTable = createTable();
  fullTable = createTable();
  println("setup done");
}

Table createTable(){
  Table fTable = new Table();
  fTable.addColumn("secs"); // This column stores a unique identifier for each record. We will just count up from 0 
  // Add columns in read order
  fTable.addColumn("V");
  fTable.addColumn("mA");
  fTable.addColumn("C");
  return fTable;
}

// Upon recieving data from the Arduino handle I/O.
void serialEvent(Serial myPort){
  try{
      recU();
  } catch (Exception e){
    println(" RecU Exception found. ");
    Apts.removeInvalidPoints();
    Cpts.removeInvalidPoints();
    Vpts.removeInvalidPoints();
  }
}

// Plot data.
void draw(){
  try{
    if (GRecord.isSelected() == true && readingCounter % plotPeriod == 1){
      background(color(255,255,255));
      Cplot.beginDraw();
      Cplot.drawBackground();
      Cplot.drawBox();
      Cplot.drawXAxis();
      Cplot.drawYAxis();
      Cplot.drawTitle();
      Cplot.drawGridLines(GPlot.BOTH);
      for (int i=0; i<Cpts.getNPoints(); i++){
        Cplot.drawPoint(Cpts.get(i));
      }
      Cplot.endDraw();
      
      Vplot.beginDraw();
      Vplot.drawBackground();
      Vplot.drawBox();
      Vplot.drawXAxis();
      Vplot.drawYAxis();
      Vplot.drawTitle();
      Vplot.drawGridLines(GPlot.BOTH);
      Vplot.setPointColor(color(0, 0, 255, 60));
      for (int i=0; i<Vpts.getNPoints(); i++){
        Vplot.drawPoint(Vpts.get(i));
      }
      Vplot.endDraw();
      
      Aplot.beginDraw();
      Aplot.drawRightAxis();
      Aplot.drawGridLines(GPlot.BOTH);
      Vplot.setPointColor(color(255, 0, 0, 20));
      for (int i=0; i<Apts.getNPoints(); i++){
        Aplot.drawPoint(Apts.get(i));
      }
      Aplot.endDraw();
    }
  } catch (Exception e){
    println(" Draw Exception found. ");
    Apts.removeInvalidPoints();
    Cpts.removeInvalidPoints();
    Vpts.removeInvalidPoints();
  }

}
