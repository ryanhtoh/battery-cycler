#include <Wire.h>
#include <SDL_Arduino_INA3221.h>

SDL_Arduino_INA3221 ina3221;
char val;
int relayA= 2;
int relayB= 4;
unsigned long prev_ms= 0;

void setup(void) {
  pinMode(relayA, OUTPUT);
  pinMode(relayB, OUTPUT);
  digitalWrite(relayA, HIGH);
  digitalWrite(relayB, HIGH);
  Serial.begin(57600);
  ina3221.begin();
}

void loop(void) {
if( millis()-prev_ms >= 100 ){
  float busvoltage1 = 0;
  float shuntvoltage1 = 0;
  float loadvoltage1 = 0;
  float current_mA1 = 0;
  float V=0;
  prev_ms=millis();
  
  busvoltage1 = ina3221.getBusVoltage_V(1);
  shuntvoltage1 = ina3221.getShuntVoltage_mV(1);
  loadvoltage1 = busvoltage1 + (shuntvoltage1 / 1000);
  current_mA1 = -ina3221.getCurrent_mA(1);  // minus is to get the "sense" right.   - means the battery is charging, + that it is discharging
  V = analogRead(0)*0.005;

  Serial.print(busvoltage1); Serial.print(", V, ");
  Serial.print(current_mA1); Serial.println(", mA");
}

  if(Serial.available() > 0){
    val = Serial.read();
    if(val=='a'){
      digitalWrite(relayA, HIGH);
    }
    else if(val=='A'){
      digitalWrite(relayA, LOW);
    }
    else if(val=='b'){
      digitalWrite(relayB, HIGH);
    }
     else if(val=='B'){
      digitalWrite(relayB, LOW);
    }
    
  }
  
}
