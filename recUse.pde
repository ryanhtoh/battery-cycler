/* =========================================================
 * Functions for recording data and sending commands to the Arduino. 
 * =========================================================
 */

// Handles I/O for the Arduino.
void recU(){
  ArduinoStr = myPort.readStringUntil('\n'); //The newline separator separates each Arduino loop. We will parse the data by each newline separator. 
  if (ArduinoStr!= null) { //We have a reading! Record it.
    ArduinoStr = trim(ArduinoStr); //gets rid of any whitespace or Unicode nonbreakable space
    String vals[] = split(ArduinoStr, ','); //parses the float outputs
    mA = Float.parseFloat(vals[2]);
    Volts = Float.parseFloat(vals[0]);
    
    if(doState == "use"){
      useText.setText(Math.round(mA*100)/100+" mA, "+Volts+" V use");
    }
    else{
      chgText.setText(Math.round(mA*100)/100+" mA, "+Volts+" V chg");
    }
    
    C += mA/refreshRate/mAtoAmps;
    //automate discharge to charge switching
    if(doState=="use" && C-Cpre > deltaU){
      doChg();
    }
    //automate charge to discharge switching
    if(doState=="chg" && C-Cpre < -deltaC){
      doUse();
    }
    
    readingCounter++;
    if(readingCounter % plotPeriod == 0){
      addGraphPoints();
    }
    if(readingCounter % savePeriod == 0){
      addFileRow(fullTable, fileName);
    }
    if(justSwitched == true && readingCounter > lastSwitched+20){
      print("switch@ "+Math.round(C*100)/100+" C, "+Math.round(J*10)/10+" J, "+Math.round(mA*100)/100+" mA, "+Volts+" V;   ");
      addGraphPoints();
      addFileRow(fullTable, fileName);
      justSwitched = false;
    }
  }
}

// Add points to the graph array.
void addGraphPoints(){
    Cpts.add( (float) readingCounter/10, (float) C);
    Apts.add( (float) readingCounter/10, (float) mA);
    Vpts.add( (float) readingCounter/10, (float) Volts);
    Cplot.setPoints(Cpts);
    Vplot.setPoints(Vpts);
    Aplot.setPoints(Apts);
}

// Adds a row to the csv table.
void addFileRow(Table tableCSV, String name){
      TableRow newRow = tableCSV.addRow();
      seconds = readingCounter/10;
      newRow.setLong("secs", seconds);//record a unique identifier (the row's index)
      newRow.setFloat("V", Volts);
      newRow.setFloat("mA", mA);    
      newRow.setDouble("C", C);
      saveTable(tableCSV, name+".csv"); // Save file to the computer. c=charge, r=resistor, i=idle, l=LED, m=motor
}

// Switches the relay to charge mode.
void doChg(){
      doState = "chg";
      Cpre=C;
      myPort.write('A');
      myPort.write('b');
      justSwitched = true;
      lastSwitched = readingCounter;
      
      print("chg@ "+Math.round(C*100)/100+" C, "+Math.round(J*10)/10+" J, "+Math.round(mA*100)/100+" mA, "+Volts+" V;   ");
      addGraphPoints();
      addFileRow(fullTable, fileName);
      addFileRow(cycleTable,fileName+"cycles");
      checkCycles();
}

// Switches the relay to discharge mode.
void doUse(){
      doState = "use";
      Cpre=C;
      myPort.write('a');
      myPort.write('B');
      justSwitched = true;
      lastSwitched = readingCounter;
      
      println("use@ "+Math.round(C*100)/100+" C, "+Math.round(J*10)/10+" J, "+Math.round(mA*100)/100+" mA, "+Volts+" V;   ");
      addGraphPoints();
      addFileRow(fullTable, fileName);
      addFileRow(cycleTable,fileName+"cycles");
      checkCycles();
}

// Disconnects the battery
void doOff(){
      myPort.write('a');
      myPort.write('b');
      doState = "off";
      justSwitched = true;
      lastSwitched = readingCounter;
  
      print("off@ "+Math.round(C*100)/100+" C, "+Math.round(J*10)/10+" J, "+Math.round(mA*100)/100+" mA, "+Volts+" V;   ");
      addGraphPoints();
      addFileRow(fullTable, fileName);
      addFileRow(cycleTable,fileName+"cycles");
}

// Increments cycles. If cycles exceed the max cycles, reset the display.
void checkCycles(){
      cycles++;
      if (cycles > DISPLAYED_CYCLES){
        cycles = 0;
        Vpts = new GPointsArray();
        Apts = new GPointsArray();
        Cpts = new GPointsArray();
      }
}
