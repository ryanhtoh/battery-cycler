# Battery Cycler
Old (2016) code for battery cycling device. ino files are for Arduino code (C), and pde files are for Processing 3.5.3 code (Java). The Arudino handles the relay switching for battery charge and discharge and the Processing code handles the data collection. Data is saved to a csv which is put on a shared network.

The high level circuit diagram is provided.

The Processing 3 GUI, when run, allows for the setting of the charge and discharge Coulomb amounts. Two csv files are generated, one with continous recording of voltage, current, and charge, and the other only recording values afer each charge or discharge.
